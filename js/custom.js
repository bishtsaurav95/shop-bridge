var $ = jQuery.noConflict();
let homeUrl = `${window.location.origin}/shop-bridge`;
var Store ={
	productListingPage : function(cancelEdit=false){
        let ajaxUrl = `${window.location.origin}/shop-bridge/component/home.html`;
        $.ajax({
            method: "GET",
            url: ajaxUrl, 
            success: function(data){
                $("#common-page").html(data);
                Store.getProducts();
                if(cancelEdit){
                    window.history.pushState("Home", "home", `${homeUrl}`);
                }
            }
        });
    },
    getProducts : function(){
        $.ajax({
            method: "GET",
            url: "https://fakestoreapi.com/products", 
            success: function(data){
                let products = data;
                let htmlAppend = $("#allProducts tbody");
                $.each( products, function( i, product ) {
                    htmlAppend.append(
                    `<tr>
                        <td class="product-title">${product.title}</td>
                        <td class="text-capitalize">${product.category}</td>
                        <td>${product.description}</td>
                        <td>$${product.price}</td>
                        <td>
                            <span id="edit-product" data-id="${product.id}" class="edit" ><i class="material-icons">edit</i></a>
                            <span id="delete-product" data-id="${product.id}" class="delete" ><i class="material-icons">delete</i></span>
                        </td>
                    </tr>`)
                });
                Store.hideLoader();
            },
            error: function(data){
                let json = $.parseJSON(data);
                alert(json.error);
            }
        });
    },
    deleteProduct : function(currentElement){
        let confirmBox = confirm(`Are you sure you want to remove ${currentElement.parents('tr').children('.product-title').text()}.`);
        if(!confirmBox){
            return
        }
        $.ajax({
            method: "DELETE",
            url: `https://fakestoreapi.com/products/${currentElement.data('id')}`, 
            success: function(data){
                if(data){
                    currentElement.parents('tr').remove();
                }
            },
            error: function(data){
                let json = $.parseJSON(data);
                alert(json.error);
            }
        });  
    },
    editProduct : function(id){
        let productId = id;
        $.ajax({
            method: "GET",
            url: `${homeUrl}/component/productForm.html`, 
            success: function(data){
                $("#common-page").html(data);
                if(!history.state){
                    window.history.pushState("Details", "Edit", `${homeUrl}/edit.html?id=${productId}`);
                }
                Store.productForm(productId);
            },
            complete: function(){
                $('.modal-title').text("Edit Product");
            }
        });
    },
    productForm : function(productId = ""){
        if(productId){
            Store.getSingleProduct(productId)
        }
    },
    getSingleProduct : function(productId){
        $.ajax({
            method: "GET",
            url: `https://fakestoreapi.com/products/${productId}`, 
            success: function(data){
                $("#name").val(data.title);
                $("#cat").val(data.category);
                $("#desc").val(data.description);
                $("#price").val(data.price);
                Store.hideLoader();
            },
            error: function(data){
                let json = $.parseJSON(data);
                alert(json.error);
            }
        });  
    },
    updateAddProduct : function(){
        let title = $.trim($("#name").val());
        let price = $.trim($("#price").val());
        let description = $.trim($("#description").val());
        let category = $.trim($("#category").val());
        if(title === "" && price === "" && description === "" && category === ""){
            alert("Please insert all the required fields");
            return false;
        }
        let productId = '';
        let searchParams = new URLSearchParams(window.location.search);
        productId =  searchParams.has('id') ?  searchParams.get('id') : "";
        let patch = {      
            title: title,
            price: price,
            description: description,
            category: category
        }
        let methodType = productId ? "PATCH" : "POST";
        $.ajax({
            method: methodType,
            url: `https://fakestoreapi.com/products/${productId}`, 
            data: JSON.stringify(patch),
            processData: false,
            contentType: 'application/json-patch+json',
            success: function(data){
                if(data){
                    let alertString = methodType === "PATCH" ? "SuccessFully Updated" : "SuccessFully Added";
                    alert(alertString)
                    $("#productForm")[0].reset();
                    
                }
            }
        });
    },
    addProduct : function() {
        $('.modal-title').text("Add Product");
        $.ajax({
            method: "GET",
            url: `${homeUrl}/component/productForm.html`, 
            success: function(data){
                $("#common-page").html(data);
                window.history.pushState("Add", "Add", `${homeUrl}/add.html`);
                Store.hideLoader();
            },
            complete: function(){
                $('.modal-title').text("Add Product");
            }
        });
    },
    initApp : function(loc){
        Store.showLoader();
        let pageName = loc.pathname.split('/').pop();
        let searchParams = new URLSearchParams(loc.search);
        if(pageName === "edit.html" && searchParams.has('id')){
            Store.editProduct(searchParams.get('id'));
        }else if(pageName === "add.html"){
            Store.addProduct(loc.href);
        }else{ 
            Store.productListingPage();
        }
    },
    showLoader : function(){
        $('body').addClass('ovf-hidden');
        $('.loader-wrp').show();
    },
    hideLoader : function(){
        $('body').removeClass('ovf-hidden');
        $('.loader-wrp').hide();
    }
}

$(document).ready(function(){
    //Store.productListingPage();
    //console.log(homeUrl);

    /**initApp For initialzing the application **/
    Store.initApp(window.location)
    $(document).on('click','#delete-product', function(){
        Store.deleteProduct($(this));
    })
    
    /**Edit Product page will be open and calling editProduct method for that**/
    $(document).on('click','#edit-product', function(){
        Store.showLoader();
        Store.editProduct($(this).data('id'),window.location.href);
    })

    /**Back button feature in which listing page will appear**/
    $(document).on('click','#cancelEdit', function(){
        Store.showLoader();
        Store.productListingPage(true);
    })

    /**updateAddProduct for updating and add product**/
    $(document).on('click','#saveItem', function(){
        Store.updateAddProduct($(this).data('id'));
        return false;
    })

    /**addProduct Method for showing add product page**/
    $(document).on('click','#addProduct', function(){
        Store.addProduct(window.location.href);
        return false;
    })
    if (window.history && window.history.pushState) {
        $(window).on('popstate', function() {
            //console.log("test")
            Store.initApp(window.location);
        });
    }
});



